import sys
import pathlib
import json

import typing as t

import jinja2 as jinja


def _get_value_or_default(a_dict: dict, key: str, default) -> t.Any:
    if key in a_dict:
        return a_dict[key]
    return default


def _findings_sort_func(finding: dict) -> str:
    return finding['src_path']


def _semgrep_json_to_html(sg_result, jinja_env) -> str:
    results = sg_result['results']

    findings = []

    for res in results:
        src_path: str = res['path']
        check_id: str = res['check_id']

        print(src_path, check_id)

        start: dict = res['start']
        end: dict = res['end']

        extra_infos = res['extra']
        severity: str = extra_infos['severity']
        fingerprint: str = extra_infos['fingerprint']
        source_lines: str = extra_infos['lines']
        message: str = extra_infos['message']

        meta_data = extra_infos['metadata']
        category: str = meta_data['category']
        confidence: t.Optional[str] = _get_value_or_default(meta_data, 'confidence', None)
        impact: str = _get_value_or_default(meta_data, 'impact', None)
        likelihood: str = _get_value_or_default(meta_data, 'likelihood', None)



        findings.append({
            'src_path': src_path,
            'check_id': check_id,
            'start': start,
            'end': end,
            'severity': severity,
            'fingerprint': fingerprint,
            'source_lines': source_lines,
            'message': message,
            'category': category,
            'confidence': confidence,
            'impact': impact,
            'likelihood': likelihood,
        })

    findings.sort(key=lambda x: x['src_path'])

    template = jinja_env.get_template('template.html')
    return template.render(findings=findings)


def main(args: t.List[str]) -> None:
    jinja_env = jinja.Environment(
        loader=jinja.FileSystemLoader(pathlib.Path(__file__).parent),
        autoescape=jinja.select_autoescape()
    )

    if len(args) != 2:
        print('Need two args - Semgrep JSON results file path and output HTML file path.', file=sys.stderr)
        sys.exit(1)

    input_file = pathlib.Path(args[0])
    output_file = pathlib.Path(args[1])

    # TODO: Ensure input file is an existing, readable file
    # TODO: Ensure output file does not exist

    input_json = json.loads(input_file.read_text())

    html = _semgrep_json_to_html(input_json, jinja_env)

    output_file.write_text(html)


if __name__ == '__main__':
    main(sys.argv[1:])
